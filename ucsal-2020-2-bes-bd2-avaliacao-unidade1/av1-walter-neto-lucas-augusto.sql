-- Avaliacao 1 / Dupla: Walter Neto e Lucas Augusto

drop table if exists proposta_matricula cascade;
drop table if exists aluno cascade;
drop table if exists disciplina cascade;
drop table if exists professor cascade;

create table proposta_matricula (
	matricula_aluno 	integer			not null,
	cod_disciplina		char(6)			not null,
	constraint pk_proposta_matricula
		primary key (matricula_aluno, cod_disciplina)
);
	
create table aluno (
	matricula			serial			not null,
	nome				varchar(40)		not null,
	telefone			varchar(11)				,
	constraint pk_aluno
		primary key (matricula)
);
	
create table disciplina (
	cod					char(6)			not null,
	nome				varchar(100)	not null,
	carga_horaria		smallint		not null,
	matricula_professor	integer			not null,
	constraint pk_disciplina
		primary key (cod)
);
	
create table professor (
	matricula			serial			not null,
	nome				varchar(40)		not null,
	data_admissao		date			not null,
	email				varchar(250)			,
	constraint pk_professor
		primary key (matricula)
);

alter table proposta_matricula
	add constraint fk_matricula_aluno
		foreign key (matricula_aluno)
		references aluno 
		on update cascade
		on delete cascade,
	add constraint fk_cod_disciplina
		foreign key (cod_disciplina)
		references disciplina;
		
alter table disciplina
	add constraint fk_matricula_professor
		foreign key (matricula_professor)
		references professor;
		
alter table disciplina
	  add constraint ch_disciplina_carga_horaria
	  check (carga_horaria = 30 or carga_horaria = 60 or carga_horaria = 90);
	  
CREATE OR REPLACE FUNCTION f_trg_insert_disciplina() 
RETURNS TRIGGER
AS $$
DECLARE 
	l_professor_nome varchar;
BEGIN
	IF (TG_OP = 'INSERT' OR TG_OP = 'UPDATE') THEN
		IF ((SELECT count(matricula_professor) FROM disciplina WHERE new.matricula_professor = matricula_professor) >= 5) THEN
			SELECT	nome
			INTO	l_professor_nome
			FROM	professor
			WHERE	matricula = new.matricula_professor;
			RAISE EXCEPTION 'O professor % pode lecionar no maximo 5 disciplinas!', l_professor_nome using errcode='23514';
  		END IF;
	RETURN NEW;
	END IF;
  RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER trg_insert_disciplina
BEFORE INSERT OR UPDATE OF matricula_professor
ON disciplina
FOR EACH ROW
EXECUTE PROCEDURE f_trg_insert_disciplina();

insert into professor (nome, data_admissao, email) values
	('Antonio Claudio Neiva', '2018-05-20', 'antonioclaudioneiva@ucsal.pro.br'),
	('Jeane Franco', '2020-02-20', 'jeanefranco@ucsal.pro.br'),
	('Andre Brasil', '2019-07-12', 'andrebrasil@ucsal.pro.br'),
	('Fernando Borges', '2019-03-06', 'fernandoborges@ucsal.pro.br'),
	('Mario Jorge', '2017-09-27', 'mariojorge@ucsal.pro.br');

insert into disciplina (cod, nome, carga_horaria, matricula_professor) values
	('BESBD2', 'Banco de Dados 2', 90, (select matricula from professor where nome = 'Antonio Claudio Neiva')),
	('BESPOO', 'Programacao Orientada a Objetos', 90, (select matricula from professor where nome = 'Antonio Claudio Neiva')),
	('BESIA', 'Inteligencia Artificial', 90, (select matricula from professor where nome = 'Antonio Claudio Neiva')),									
	('BESTQS', 'Teste e Qualidades de Software', 60, (select matricula from professor where nome = 'Antonio Claudio Neiva')),
	('BESGCM', 'Gestao e Configuracao de Mudancas', 30, (select matricula from professor where nome = 'Antonio Claudio Neiva')),
	('BESLPA', 'Linguagem de Programacao e Algoritmos', 30, (select matricula from professor where nome = 'Fernando Borges')),
	('BESEDD', 'Estrutura de Dados', 60, (select matricula from professor where nome = 'Mario Jorge'));
	
insert into aluno (nome, telefone) values
	('Walter Neto', '1234-5678'),
	('Lucas Augusto', '5678-1234'),
	('Haroldo Beyer', '8765-4321'),
	('Mateus Mendes', '1379-9731'),
	('Jean Xavier', '1472-5836');
										  
insert into proposta_matricula (matricula_aluno, cod_disciplina) values
	((select matricula from aluno where nome = 'Walter Neto'), (select cod from disciplina where nome = 'Banco de Dados 2')),
	((select matricula from aluno where nome = 'Walter Neto'), (select cod from disciplina where nome = 'Inteligencia Artificial')),
	((select matricula from aluno where nome = 'Lucas Augusto'), (select cod from disciplina where nome = 'Teste e Qualidades de Software')),
	((select matricula from aluno where nome = 'Lucas Augusto'), (select cod from disciplina where nome = 'Banco de Dados 2')),
	((select matricula from aluno where nome = 'Haroldo Beyer'), (select cod from disciplina where nome = 'Gestao e Configuracao de Mudancas')),
	((select matricula from aluno where nome = 'Jean Xavier'), (select cod from disciplina where nome = 'Programacao Orientada a Objetos')),
	((select matricula from aluno where nome = 'Mateus Mendes'), (select cod from disciplina where nome = 'Estrutura de Dados'));

drop view if exists v_professor_disciplina;

create or replace view v_professor_disciplina
as	
	select p.nome as nome_professor,
		p.email as email_professor,
		coalesce(d.nome,'(nenhuma disciplina associada)') as nome_disciplina
	from professor p
	left outer join disciplina d on (p.matricula = d.matricula_professor)
	order by p.nome asc;
;

select a.nome as nome_aluno,
		a.matricula as matricula_aluno,
		sum(d.carga_horaria) as carga_horaria_total
	from aluno a
	inner join proposta_matricula pm on (a.matricula = pm.matricula_aluno)
	inner join disciplina d on (pm.cod_disciplina = d.cod)
	group by a.nome, a.matricula
	having sum(d.carga_horaria) < 150
	order by sum(d.carga_horaria) asc;

select p.nome as nome_professor,
		p.data_admissao as data_admissao_professor
	from professor p
	inner join disciplina d on (p.matricula = d.matricula_professor)
	left join proposta_matricula pm on (d.cod = pm.cod_disciplina)
	where not exists (select * from proposta_matricula where pm.cod_disciplina = cod_disciplina)
	order by p.nome asc;
