-- criação das tabelas - início

-- item: id (serial), nome (string)
create table item (
	id			serial			not null,
	nome		varchar(40)		not null,
	constraint pk_item
		primary key (id));

-- movimento: id (serial), data (date), id_item (integer), qtd (integer)
create table movimento (
	id			serial			not null,
	data		date			not null,
	id_item		integer			not null,
	qtd			integer			not null,
	constraint pk_movimento
		primary key (id));
		
-- saldo: data (date), id_item (integer), qtd (integer)
create table saldo (
	data		date			not null,
	id_item		integer			not null,
	qtd			integer			not null,
	constraint pk_saldo
		primary key (data, id_item));

-- chaves estrangeiras
alter table movimento
	add constraint fk_movimento_item
		foreign key (id_item)
		references item;
		
alter table saldo
	add constraint fk_saldo_item
		foreign key (id_item)
		references item;

-- criação das tabelas - fim 
-- criar trigger para inclusão - início

-- Evento: inclusão
-- Comportamento: atualizar o saldo a partir da inclusão de movimentos

-- criar a função do trigger -  versão NÃO refatorada
create or replace function f_trg_ins_movimento ()
returns trigger
AS
$$
declare 
	v_data_saldo_anterior date;
	v_qtd_saldo_anterior integer;
begin
	-- new é registro da tabela movimento que está sendo inserido no momento

	-- inserir o saldo na data/item do movimento, caso não exista um saldo nesta data
	if not exists (select * from saldo where id_item = new.id_item and data = new.data) then
		-- a maior data de saldo que seja anterior à data do movimento que está sendo inserido (para o item deste movimento)
		select	max(data)
		into 	v_data_saldo_anterior
		from	saldo
		where	id_item	= new.id_item
		and		data < new.data;

		-- obter a quantidade do saldo na data do saldo anterior para item que está sendo inserido
		select	qtd
		into 	v_qtd_saldo_anterior
		from	saldo
		where	id_item = new.id_item
		and		data	= v_data_saldo_anterior;

		-- inserir novo saldo na data do movimento, para o item, somando o saldo anterior com a quantidade do movimento
		insert into saldo (data, id_item, qtd)
		values (new.data, new.id_item, coalesce(v_qtd_saldo_anterior, 0) + new.qtd);
		
		-- atualizar os saldos a partir da data do movimento
		update 	saldo
		set		qtd = qtd + new.qtd
		where	id_item = new.id_item
		and		data > new.data;
		
	else
		-- caso já exista saldo na data, então o mesmo de ser atualizado
		update 	saldo
		set		qtd = qtd + new.qtd
		where	id_item = new.id_item
		and		data >= new.data;
	end if;
	
	return new;
end;
$$
language PLPGSQL;

-- criar a função do trigger -  VERSÃO REFATORADA
drop function if exists f_trg_ins_movimento () cascade;

create or replace function f_trg_ins_upd_del_movimento ()
returns trigger
AS
$$
declare 
	v_qtd_saldo_anterior integer;
	
begin
	-- new é registro da tabela movimento que está sendo inserido no momento

	-- tratamento para operações de insert or update	
	if TG_OP = 'INSERT' OR TG_OP = 'UPDATE' then 
	
		-- inserir o saldo na data/item do movimento, caso não exista um saldo nesta data
		if not exists (select * from saldo where id_item = new.id_item and data = new.data) then

			-- obter a quantidade do saldo na data do saldo anterior para item que está sendo inserido
			select	s1.qtd
			into 	v_qtd_saldo_anterior
			from	saldo s1
			where	s1.id_item = new.id_item
			and		s1.data	= (select 	max(s2.data)
							   from 	saldo s2
							   where	s2.id_item 	= new.id_item
							   and		s2.data 	< new.data);

			-- inserir novo saldo na data do movimento, para o item, com a quantidade do saldo anterior
			insert into saldo (data, id_item, qtd)
			values (new.data, new.id_item, coalesce(v_qtd_saldo_anterior, 0));

		end if;

		-- adicionar a qtd insert/update nas qtds para os saldos com data maior ou igual à data do movimento
		update 	saldo
		set		qtd = qtd + new.qtd
		where	id_item = new.id_item
		and		data >= new.data;
	end if;
	
	if TG_OP = 'DELETE' OR TG_OP = 'UPDATE' then
	
		-- reduzir a qtd insert/update nas qtds para os saldos com data maior ou igual à data do movimento
		update 	saldo
		set		qtd = qtd - old.qtd
		where	id_item = old.id_item
		and		data >= old.data;
	end if;

	if TG_OP = 'INSERT' OR TG_OP = 'UPDATE' then 
		return new;
	else
		return old;
	end if;
end;
$$
language PLPGSQL;

-- criar o trigger
drop trigger if exists trg_ins_movimento on movimento;
drop trigger if exists trg_ins_upd_del_movimento on movimento;

create trigger trg_ins_upd_del_movimento
before insert or update of data, id_item, qtd or delete
on movimento
for each row
execute procedure f_trg_ins_upd_del_movimento();

-- criar trigger para inclusão - fim

-- testar o trigger - início

delete	
from	movimento;

delete
from	saldo;

insert into item (nome) values
('parafuso'),
('porca'),
('chave de fenda'),
('martelo');

insert into movimento (data, id_item, qtd) values
('2020-09-21',(select id from item where nome='porca'),100),
('2020-09-21',(select id from item where nome='porca'),70),
('2020-09-21',(select id from item where nome='martelo'),10),
('2020-09-23',(select id from item where nome='porca'),-8),
('2020-09-23',(select id from item where nome='porca'),5),
('2020-09-25',(select id from item where nome='martelo'),-2),
('2020-09-27',(select id from item where nome='parafuso'),500),
('2020-09-27',(select id from item where nome='porca'),-12);

select	*
from	saldo
order by id_item, 
		 data;

-- saída esperada (conteúdo da tabela saldo):
-- 21/09/2020	porca 		170
-- 21/09/2020	martelo 	 10
-- 23/09/2020	porca		167
-- 25/09/2020	martelo		  8
-- 27/09/2020	parafuso	500
-- 27/09/2020	porca		155

select	*
from	saldo;

-- testar o trigger - fim


-- testar trigger para insert, passo a passo para o insert

delete	
from	movimento;
delete
from	saldo;

insert into movimento (data, id_item, qtd) values
('2020-09-21',(select id from item where nome='porca'),100);

insert into movimento (data, id_item, qtd) values
('2020-09-21',(select id from item where nome='porca'),3);

insert into movimento (data, id_item, qtd) values
('2020-09-23',(select id from item where nome='porca'),5);

insert into movimento (data, id_item, qtd) values
('2020-09-27',(select id from item where nome='porca'),-12);

insert into movimento (data, id_item, qtd) values
('2020-09-25',(select id from item where nome='porca'),10);

insert into movimento (data, id_item, qtd) values
('2020-09-23',(select id from item where nome='porca'),1);

select	*
from	saldo
order by id_item, 
		 data;

-- testar trigger para update, passo a passo para o insert

select	*
from	saldo
where	id_item = 2
order by id_item, 
		 data;

select  *
from	movimento
where	id_item = 2
order by id_item, 
		 data;

-- movimentos:
-- 55	"2020-09-21"	2	100
-- 56	"2020-09-21"	2	 70
-- 58	"2020-09-23"	2	 -8
-- 59	"2020-09-23"	2	  5
-- 62	"2020-09-27"	2	-12

-- saldos atuais
-- "2020-09-21"	2	170
-- "2020-09-23"	2	167
-- "2020-09-27"	2	155

update 	movimento
set		qtd = 68
where	id = 56;

-- saldos esperados
-- "2020-09-21"	2	168
-- "2020-09-23"	2	165
-- "2020-09-27"	2	153

delete 
from	movimento
where 	id = 59;

-- saldos esperados
-- "2020-09-21"	2	168
-- "2020-09-23"	2	160
-- "2020-09-27"	2	148

delete
from	movimento;

select	*
from	saldo
order by id_item, 
		 data;
