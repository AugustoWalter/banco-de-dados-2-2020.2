create table leitor (
	matricula 	serial 		not null,
	nome 		varchar(40) not null,
	telefone 	varchar(11) not null,
	constraint pk_leitor
		primary key (matricula));

create table livro (
	isbn		char(13) 		not null,
	titulo 		varchar(200) 	not null,
	tipo 		char(1) 		not null,
	constraint pk_livro
		primary key (isbn),
	constraint ch_livro_tipo
		check (tipo in ('E','C')));

create table emprestimo (
	id 					serial 			not null,
	isbn_livro 			char(13) 		not null,
	data_emprestimo	 	date 			not null,
	data_devolucao 		date 			null,
	matricula_leitor 	integer 		not null,
	constraint pk_emprestimo
		primary key (id),
	constraint ch_emprestimo_datas
		check (data_devolucao > data_emprestimo));

alter table emprestimo
	add constraint fk_emprestimo_livro
		foreign key (isbn_livro)
		references livro,
	add constraint fk_emprestimo_leitor
		foreign key (matricula_leitor)
		references leitor;
		
insert into leitor (nome, telefone) values
	('Claudio Neiva' ,'71988776655'),
	('Maria da Silva' ,'71977665544'),
	('Pedro Santos' ,'75989067856');
	
insert into livro (isbn, titulo, tipo) values
	('9788533302273','Plantando Caju','C'),
	('7575755655555','Colhendo Caju','C'),
	('7272262252242','Vendendo Caju','E'),
	('7177167178763','Receitas com Caju','E'),
	('7576567365356','Segredos do Caju','C');		


-- 1) Crie um trigger que impeça que um livro para consulta seja emprestado, ou seja, um trigger
-- que impeça um insert/update na tabela de emprestimo para livros cujo tipo seja C. Dica:
-- pesquise sobre raise exception.

create or replace function f_trg_ins_upd_emprestimo()
returns trigger
as
$$
declare 
	l_titulo_livro varchar;
begin
	if exists(select * from livro where isbn = new.isbn_livro and tipo = 'C') then
		select	titulo
		into	l_titulo_livro
		from	livro
		where	isbn = new.isbn_livro;
		raise exception 'Não é possível realizar empréstimo para o livro %, pois o mesmo é apenas para consulta.', l_titulo_livro using errcode='23514';
	end if;
	return new; -- pq trata-se de um trigger de insert/update
end;
$$
language PLPGSQL;

drop trigger if exists trg_ins_upd_emprestimo on emprestimo;

create trigger trg_ins_upd_emprestimo
before insert or update of isbn_livro
on emprestimo
for each row
execute procedure f_trg_ins_upd_emprestimo();


-- Para testes, considere que o seguinte script deve ter sucesso, pois contém apenas livros do tipo ‘E’ (livros para empréstimo):

insert into emprestimo (isbn_livro, data_emprestimo, data_devolucao, matricula_leitor)
values
	('7272262252242','2020-09-01','2020-09-08',(select matricula from leitor where nome = 'Claudio Neiva')),
	('7177167178763','2020-09-01','2020-09-10',(select matricula from leitor where nome = 'Maria da Silva')),
	('7272262252242','2020-09-10',null,(select matricula from leitor where nome = 'Maria da Silva')),
	('7177167178763','2020-09-15','2020-09-17',(select matricula from leitor where nome = 'Pedro Santos'));


-- Para testes, considere que o seguinte script deve FALHAR, pois contém um livro do tipo ‘C’ (livro para consulta):

insert into emprestimo (isbn_livro, data_emprestimo, data_devolucao, matricula_leitor)
values
	('7272262252242','2020-10-05','2020-10-09',(select matricula from leitor where nome =  'Pedro Santos')),
	('7576567365356','2020-10-06',null,(select matricula from leitor where nome = 'Claudio Neiva'));


-- Consultar a lista de empréstimos, confirmando que nenhum dos dois empréstimos (conjunto de testes 2), foi armazenado:

select	*
from	emprestimo;


-- Este update NÃO dispara o trigger, pois o trigger foi configurado para "update of isbn_livro":

update emprestimo
set	data_devolucao = '2020-09-15'
where id = 3;

-- Este update dispara o trigger, pois o trigger foi configurado para "update of isbn_livro":

update emprestimo
set	isbn_livro = '7576567365356'
where id = 3;
