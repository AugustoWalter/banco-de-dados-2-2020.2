drop table if exists nota_fiscal cascade;
drop table if exists item_nota_fiscal cascade;
drop table if exists item_estoque cascade;

create table item_estoque (
	id				serial		not null,
	nome			varchar(40)	not null,
	qtd_disponivel	integer		not null,
	constraint pk_item_estoque
		primary key (id),
	constraint ch_item_estoque_qtd_disponivel
		check (qtd_disponivel >= 0));

create table nota_fiscal (
	numero		serial		not null,
	data		date		not null,
	cpf_cliente	char(11)	not null,
	constraint pk_nota_fiscal
		primary key (numero));

create table item_nota_fiscal (
	id						serial			not null,
	numero_nota_fiscal		integer			not null,
	id_item					integer			not null,
	qtd						integer			not null,
	vl_unitario				numeric(10,2)	not null,
	constraint pk_item_nota_fiscal
		primary key (id));
		
alter table item_nota_fiscal	
	add constraint fk_item_nota_fiscal_nf
		foreign key (numero_nota_fiscal)
		references nota_fiscal
		on delete cascade
		on update cascade,
	add constraint fk_item_nota_fiscal_item
		foreign key (id_item)
		references item_estoque
		on update cascade;

drop view v_item_nota_fiscal;

-- não ocupa espaço (os dados NÃO são pré-armazenados);
-- a consulta da view é pré-analizada e "anexada" à consulta que utiliza a view.
create or replace view v_item_nota_fiscal
as	
	select	nf.numero as numero_nf,
			nf.data,
			nf.cpf_cliente,
			ie.nome,
			inf.qtd,
			inf.vl_unitario,
			(inf.qtd * inf.vl_unitario) as vl_total
	from	nota_fiscal	nf
	inner join item_nota_fiscal inf on (inf.numero_nota_fiscal = nf.numero)
	inner join item_estoque ie on (ie.id = inf.id_item);
	

drop materialized view vm_item_nota_fiscal;

-- os dados da consulta da view são previamente armazenados.
create materialized view vm_item_nota_fiscal
as	
	select	nf.numero as numero_nf,
			nf.data,
			nf.cpf_cliente,
			ie.nome,
			inf.qtd,
			inf.vl_unitario,
			(inf.qtd * inf.vl_unitario) as vl_total
	from	nota_fiscal	nf
	inner join item_nota_fiscal inf on (inf.numero_nota_fiscal = nf.numero)
	inner join item_estoque ie on (ie.id = inf.id_item);




insert into item_estoque (nome, qtd_disponivel) values
('vela',30),
('óleo',100),
('amortecedor',10);

insert into nota_fiscal (data, cpf_cliente) values 
('2020-09-01','12345678900'),
('2020-09-01','23412345677');

select 	*
from	nota_fiscal;

insert into item_nota_fiscal (numero_nota_fiscal, id_item, qtd, vl_unitario) values
(1,(select id from item_estoque where nome='vela'),4,30.50),
(1,(select id from item_estoque where nome='amortecedor'),2,268.45),
(2,(select id from item_estoque where nome='óleo'),4,37.73),

-- a inserção poderia ser tbm desse jeito
--insert into item_nota_fiscal (numero_nota_fiscal, id_item, qtd, vl_unitario) values
--((select numero from nota_fiscal where cpf_cliente='23412345677'),(select id from item_estoque where nome='vela'),4,37.73);

-- consulta que utiliza view
select	*
from	v_item_nota_fiscal
where 	numero_nf = 1;
-- durante a execução é como se fosse:
	select	nf.numero as numero_nf,
			nf.data,
			nf.cpf_cliente,
			ie.nome,
			inf.qtd,
			inf.vl_unitario,
			(inf.qtd * inf.vl_unitario) as vl_total
	from	nota_fiscal	nf
	inner join item_nota_fiscal inf on (inf.numero_nota_fiscal = nf.numero)
	inner join item_estoque ie on (ie.id = inf.id_item)
	where	(nf.numero) = 1;

-- consulta que utiliza view materialized
select	*
from	vm_item_nota_fiscal
where 	numero_nf = 1;
-- existe o dado pré-armazenado
-- funciona como uma consulta regular à uma tabela "vm_item_nota_fiscal"
-- desvantagem é que qualquer novo dado que seja inserido nas tabelas não será percebido pela view materializada
insert into item_nota_fiscal (numero_nota_fiscal, id_item, qtd, vl_unitario) values
(1,(select id from item_estoque where nome='óleo'),4,37.73);
-- consulta à view depois do insert
select	*
from	v_item_nota_fiscal;
-- consulta à view materializada depois do insert
select	*
from	vm_item_nota_fiscal;
-- podemos atualizar os dados da view materializada
refresh materialized view vm_item_nota_fiscal;
-- consulta à view materializada depois do refresh
select	*
from	vm_item_nota_fiscal;


-- exemplo de trigger

create or replace function f_trg_insert_item_nota_fiscal()
returns trigger
as
$$
declare	
	l_qtd_disponivel_item	integer;
	l_nome_item	varchar;
begin

	-- carregar nas variável locais ao trigger os dados da tabela de item_estoque
	select	nome,
			qtd_disponivel
	into	l_nome_item,
			l_qtd_disponivel_item
	from	item_estoque
	where	id = new.id_item;

	-- validar a quantidade disponível do item (campo da tabela de item de estoque)
	if new.qtd > l_qtd_disponivel_item then
		--raise exception sqlstate '23514';
		raise exception 'Não é possível realizar a venda pois o % tem apenas % itens em estoque.', l_nome_item, l_qtd_disponivel_item using errcode='23514', hint='Reduza a quantidade para poder realizar a venda.';
	end if;
	
	-- atualizar a quantidade disponível na tabela de item de estoque
	update	item_estoque
	set		qtd_disponivel = qtd_disponivel - new.qtd
	where	id = new.id_item;
	
	return new;	
end;
$$
language PLPGSQL;

create trigger trg_insert_item_nota_fiscal
before insert
on item_nota_fiscal
for each row
execute procedure f_trg_insert_item_nota_fiscal();



-- teste do trigger

insert into item_estoque (nome, qtd_disponivel) values
('vela',30),
('óleo',100),
('amortecedor',10);

select *
from item_estoque;

-- venda 1 (sucesso). Todos os itens estão disponíveis e o estoque é então atualizado.
insert into nota_fiscal (data, cpf_cliente) values 
('2020-09-01','52645698927');

select 	*
from	nota_fiscal;

insert into item_nota_fiscal (numero_nota_fiscal, id_item, qtd, vl_unitario) values
(4,(select id from item_estoque where nome='vela'),20,30.50),
(4,(select id from item_estoque where nome='amortecedor'),6,268.45);

-- venda 2 (falha). Um dos itens não está disponível na quantidade necessária e estoque NÃO será atualizado, pois a operação será cancelada.
insert into nota_fiscal (data, cpf_cliente) values 
('2020-09-01','77766355588');

select 	*
from	nota_fiscal;

insert into item_nota_fiscal (numero_nota_fiscal, id_item, qtd, vl_unitario) values
(5,(select id from item_estoque where nome='vela'),4,30.50),
(5,(select id from item_estoque where nome='amortecedor'),8,268.45);

-- a constraint de check é importante, pois evita que insert/update atualizem o estoque para uma quantidade inválida
-- a mensagem produzida, no entanto, não é muito "amigável"
insert into item_estoque (nome, qtd_disponivel) values
('caju',-5);





