-- aula 5

-- Views

-- View fabricante e modelo

-- drop view if exists v_fabricante_modelo;
create or replace view v_fabricante_modelo -- o replace pode ser aplicado apenas depois do from
as
select f.cnpj,
       f.nome as nome_fabricante,
	   f.telefone,
	   coalesce (m.nome, '(sem modelo cadastrado)') as nome_modelo
from fabricante f
left outer join modelo m on (f.cnpj = m.cnpj_fabricante)
	   
--select nome_fabricante
select *
from v_fabricante_modelo;
--where nome_modelo = 'Gol';

-- View modelo (para atualização)
select *
from modelo;

create or replace view v_modelo
as
select nome,
      cnpj_fabricante
from  modelo	
where cnpj_fabricante in ('59275792000150', '74779200015077')
WITH LOCAL CHECK OPTION; -- restrigindo alterações/manipulações a somente cnpj_fabricante citados no where.

select *
from v_modelo;

insert into v_modelo(nome, cnpj_fabricante) values -- funcionou pq foi manipulado antes de colocar o WITH LOCAL CHECK OPTION
  ('Fusca', '59104422005704');

insert into v_modelo(nome, cnpj_fabricante) values  -- n vai funcionar
  ('Gurgel', '5BCE57920001A0');


-- View materialized, guarda a execução da query. Diferent da view comum que só guarda a query, a View materialized 
-- armazena os dados como se fosse uma tabela.
create materialized view vm_fabricante_modelo
as
select 	f.cnpj,
		f.nome as nome_fabricante,
		f.telefone,
		coalesce(m.nome,'(sem modelo cadastrado)') as nome_modelo
from fabricante f
left outer join modelo m on (f.cnpj = m.cnpj_fabricante);

select *
from vm_fabricante_modelo;

update fabricante
set nome = 'Volkswagem do Brasil SA'
where nome = 'Volkswagem';

select *
from fabricante;

select *
from vm_fabricante_modelo;

REFRESH MATERIALIZED VIEW vm_fabricante_modelo; -- atualiza a view com relação a base de dados

select *
from vm_fabricante_modelo;

-- Não faça assim, pois view materializada é melhor:
-- Pois uma tabela criada a partir de uma consulta não permite refresh.

CREATE TABLE tab_modelo_fabricante AS 
select 	f.cnpj,
		f.nome as nome_fabricante,
		f.telefone,
		coalesce(m.nome,'(sem modelo cadastrado)') as nome_modelo
from fabricante f
left outer join modelo m on (f.cnpj = m.cnpj_fabricante);

select *
from tab_modelo_fabricante;


-- Custo da operação

CREATE EXTENSION file_fdw;

CREATE SERVER local_file FOREIGN DATA WRAPPER file_fdw;

CREATE FOREIGN TABLE words (word text NOT NULL)
SERVER local_file
OPTIONS (filename '/usr/share/dict/words');

CREATE MATERIALIZED VIEW wrd AS SELECT * FROM words;

CREATE UNIQUE INDEX wrd_word ON wrd (word);

select *
from words
where word = 'caju';
-- cost=1898.81

select *
from wrd
where word = 'caju';
-- cost=1797.01
-- cost=8.44


-- Trigger

CREATE OR REPLACE FUNCTION f_trg_upd_fabricante()
RETURNS trigger
AS $$
BEGIN 
    if length(new.telefone) < 8 then
	   raise exception SQLSTATE '22000' USING HINT = 'Telefone Inválido!';
    end if;
    raise notice 'cnpj antes % ; cnpj depois %', OLD.cnpj, NEW.cnpj;
	raise notice 'telefone antes % ; telefone depois %', OLD.telefone, NEW.telefone;
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;
	
-- O controle de tamanho mínimo para o campo telefone é melhor implementado utilizando a constraint de check
-- alter table fabricante
-- 	add constraint ch_fabricante_telefone
-- 		check (length(telefone)>=8);

CREATE TRIGGER trg_upd_fabricante -- gatilho
BEFORE UPDATE -- tipo evento; o momento em relação ao evento
ON fabricante -- objeto monitorado
FOR EACH ROW -- disparar para a cada linha que muda na tabela, ou para apenas uma setença sql que for executada.
EXECUTE PROCEDURE f_trg_upd_fabricante(); -- função de trigger útil a ser executada

-- OLD
-- "5BCE57920001A0"	"Veículos do Brasil SA"	"43563465     "	"Av. Feira"	"2769"	"Edf. Qualque Coisa"	"Liberdade"	"FDS"
-- "74779200015077"	"Tesla do Brasil SA"	"946756756    "	"Av. Bahia"	"5656"	"Edf. Uma Coisa"	"Moca"	"FDS"
update fabricante 
set telefone = '71' || telefone
where cnpj in ('74779200015077', '5BCE57920001A0' );
-- NEW
-- "5BCE57920001A0"	"Veículos do Brasil SA"	"7143563465   "	"Av. Feira"	"2769"	"Edf. Qualque Coisa"	"Liberdade"	"FDS"
-- "74779200015077"	"Tesla do Brasil SA"	"71946756756  "	"Av. Bahia"	"5656"	"Edf. Uma Coisa"	"Moca"	"FDS"

-- OLD
-- "5BCE57920001A0"	"Veículos do Brasil SA"	"7143563465   "	"Av. Feira"	"2769"	"Edf. Qualque Coisa"	"Liberdade"	"FDS"
-- "74779200015077"	"Tesla do Brasil SA"	"71946756756  "	"Av. Bahia"	"5656"	"Edf. Uma Coisa"	"Moca"	"FDS"
update fabricante 
set telefone = '55' || telefone
where cnpj in ('74779200015077', '5BCE57920001A0' );
-- NEW
-- "5BCE57920001A0"	"Veículos do Brasil SA"	"557143563465   "	"Av. Feira"	"2769"	"Edf. Qualque Coisa"	"Liberdade"	"FDS"
-- "74779200015077"	"Tesla do Brasil SA"	"5571946756756  "	"Av. Bahia"	"5656"	"Edf. Uma Coisa"	"Moca"	"FDS"

select *
from fabricante;


select CURRENT_USER, CURRENT_TIMESTAMP, CURRENT_TIME, CURRENT_DATE; -- variaveis de ambiente uteis para trigger de auditoria.

update fabricante
set telefone = '55123'
where cnpj = '74779200015077';

-- dados derivados ##use trigger##

-- cod_item, qtd, valor_untario, valor_total
-- movimento (data, operacao[D|C], valor)
-- saldo atual = saldo inicial + acumulado de todas as operações (movimentos)
