-- Listar veículos:
-- Colunas: quantidade, quilometragem do mais rodado; 
-- quilometragem do menos rodado; quilometragem média; 
-- total de quilômetros rodados por todos os veículos

select count(*) as qtd_veiculos,
max(km_atual) as km_max,	
min(km_atual) as km_min,
avg(km_atual) as km_media,
sum(km_atual) as km_total
from veiculo;