--Listar fabricante:
--Colunas: endereço (logradouro todo em minúsculo, primeiros 10 caracteres do complemento); 
--nome do fabricante todo em maiúsculo;
--Filtro: cnpj com menos de 14 caracteres 
- (lembre-se de remover os caracteres em branco do início e fim do campo durante a consulta) 
--OU que possuam caracteres não numéricos;


selecione inferior (logradouro) || '-' || substr (complemento, 1, 10) como endereco,
superior (nome)
do fabricante
onde (comprimento (trim (cnpj)) <14 ou cnpj ~ * '[az]');
-- cnpj que em sua composição tenha também caracteres.
ou

selecione inferior (logradouro) || '-' || substr (complemento, 1, 10) como endereco,
superior (nome)
do fabricante
onde comprimento (trim (cnpj)) <14
ou cnpj ~ '[^ \ d]';
-- cnpj que em sua composição nao^ seja totalmente numerico.