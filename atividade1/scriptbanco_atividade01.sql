-- Excluir as tabelas (caso existam) e, para evitar problemas de dependência de chave estrangeira 
-- na ordem das exclusões, excluir as chaves estrangeiras da tabela antes de excluir as tabelas;

drop table if exists estado cascade;
drop table if exists cidade cascade;
drop table if exists fabricante cascade;
drop table if exists modelo cascade;
drop table if exists grupo cascade;
drop table if exists veiculo cascade;
drop table if exists acessorio cascade;
drop table if exists veiculo_acessorio cascade;
drop table if exists tabela_preco cascade;

-- Criar tabelas, com suas chaves primárias e restrições de unicidade

create table estado (
	sigla			char(2)			not null,
	nome			varchar(40)		not null,
	constraint pk_estado
		primary key (sigla));
		
create table cidade (
	sigla			char(3)			not null,
	nome			varchar(40)		not null,
	sigla_estado	char(2)			not null,
	constraint pk_cidade
		primary key (sigla));

create table fabricante (
	cnpj			char(14)		not null,
	nome			varchar(40)		not null,
	telefone		char(13)		not null,
	logradouro		varchar(40)		not null,
	numero			varchar(5)		not null,
	complemento		varchar(20)			null,
	bairro			varchar(40)		not null,
	sigla_cidade	char(3)			not null,
	constraint pk_cnpj
		primary key (cnpj));
	
create table modelo (
	codigo			serial			not null,
	nome			varchar(40)		not null,
	cnpj_fabricante char(14)		not null,
	constraint pk_modelo
		primary key (codigo));
		
-- Serial = diz que o atributo é do datatype Integer, cria uma sequence e associa essa sequence como default para o atributo.
		
create table grupo (
	codigo			serial			not null,
	nome			varchar(40)		not null,
	constraint pk_grupo
		primary key (codigo));
		
create table veiculo (
	placa			char(7)			not null,
	ano_fabricacao	integer			not null,
	data_aquisicao	date			not null,
	data_venda		date			 	null,
	km_atual		integer			not null,
	chassi			char(17)		not null,
	codigo_modelo	integer			not null,
	codigo_grupo	integer			not null,
	constraint pk_veiculo
		primary key (placa),
	constraint un_veiculo_chassi
		unique (chassi));

create table acessorio (
	sigla			char(2)			not null,
	nome			varchar(20)		not null,
	constraint pk_acessorio
		primary key (sigla));
		
create table veiculo_acessorio (
	placa_veiculo	char(7)			not null,
	sigla_acessorio	char(2)			not null,
	constraint pk_veiculo_acessorio
		primary key (placa_veiculo, sigla_acessorio));

create table tabela_preco (
	codigo_grupo	integer			not null,
	data_inicio		date			not null,
	data_fim		date				null,
	valor_diaria	numeric(10,2)	not null,
	constraint pk_tabela_preco
		primary key (codigo_grupo, data_inicio));
		
create table funcionario (
	 cpf        char(11)     not null,
	 nome       varchar(40)  not null,
	 telefone   char(13)     not null,
	 constraint  pk_funcionario
	      primary key(cpf));


create table dependente (
	cpf_funcioario   char(11)     not null,
	cpf              char(11)     not null, 
	nome             varchar(40)  not null,
	telefone         char(13)     not null,
	constraint pk_dependente
	  primary key (cpf));
 

-- Alterar tabelas, adicionando às mesmas as chaves estrangeiras		

alter table cidade
	add constraint fk_cidade_estado
		foreign key (sigla_estado)
		references estado;
	
--		on update restrict	- não permite atualização dos valores das chaves primárias que são referenciados por registros de chave estrangeira (comportamento padrão)
--		on update cascade	- ao atualizar a chave primária, atualiza as chaves estrangeiras correspondentes
--		on update set null	- ao atualizar a chave primária, define como null as chaves estrangeiras correspondentes

--		on delete restrict	- não permite exclusão de tuplas cujas chaves primárias são referenciadas por registros de chave estrangeira (comportamento padrão)
--		on delete cascade	- exclui tuplas de chave estrageira que estejam associadas à tupla da chave primária que foi excluída
--		on delete set null	- define como null a chave estrangeira cuja tupla de chave primária foi excluída

alter table fabricante
	add constraint fk_fabricante_cidade
		foreign key (sigla_cidade)
		references cidade;
	
alter table modelo
	add constraint fk_modelo_fabricante
		foreign key (cnpj_fabricante)
		references fabricante;
		
alter table veiculo
	add constraint fk_veiculo_modelo
		foreign key (codigo_modelo)
		references modelo,
	add constraint fk_veiculo_grupo
		foreign key (codigo_grupo)
		references grupo;
		
alter table veiculo
	  add constraint ch_veiculo_data_venda
	  check (data_venda >= data_aquisicao);

		
alter table veiculo_acessorio
	add constraint fk_veiculo_acessorio_veiculo
		foreign key (placa_veiculo)
		references veiculo
		on update cascade
		on delete cascade,
	add constraint fk_veiculo_acessorio_acessorio
		foreign key (sigla_acessorio)
		references acessorio;
		
 

alter table tabela_preco
	add constraint fk_tabela_preco_grupo
		foreign key (codigo_grupo)
		references grupo;
		

alter table dependente
        add constraint fk_dependente_funcionario
       foreign key (cpf_funcioario)
       references funcionario;		

-- inserção de dados para teste

insert into estado (sigla, nome) 
values 
	('BA','Bahia'),
	('RJ','Rio de Janeiro'),
	('SP','São Paulo');

insert into cidade  (sigla, nome, sigla_estado)
values
	('SSA','Salvador','BA'),
	('FDS','Feira de Santana','BA'),
	('ALG','Alagoinhas','BA'),
	('PRP','Petrópolis','RJ'),
	('SBC','São Bernardo do Campo','SP'),
	('SCS','São Caetano do Sul','SP');

insert into fabricante (cnpj, nome, telefone, logradouro, numero, complemento, bairro, sigla_cidade)
values 
	('59104422005704','Volkswagem','08000195775','Via Anchieta','s/n','KM23','Centro','SBC'),
	('59275792000150','General Motors do Brasil Ltda','08007270640','Av. Goiás','2769',null,'Santa Paula','SCS'),
	('747792000150','Tesla do Brasil SA','46756756','Av. Bahia','5656','Edf. Uma Coisa','Moca','FDS'),
	('5BCE57920001A0','Veículos do Brasil SA','43563465','Av. Feira','2769','Edf. Qualque Coisa','Liberdade','FDS');


insert into modelo (nome, cnpj_fabricante)
values 
	('Gol','59104422005704'),
	('Polo','59104422005704'),
	('Virtus','59104422005704'),
	('Celta','59275792000150'),
	('Cruze','59275792000150'),
	('Ka','59104422005704'), 
	('Golf','59104422005704'), 
	('Uno','59104422005704'), 
	('Fusca','59275792000150'),
	('Brasilia','59275792000150');
	
insert into grupo (nome)
values 
	('Básico'),
	('Luxo');

insert into veiculo (placa, ano_fabricacao, data_aquisicao, data_venda, km_atual, chassi, codigo_modelo, codigo_grupo)
values 
	('ABC1234',2017,'2018-10-05',null,			80015,'23445643FGX23345',(select codigo from modelo where nome = 'Gol'), (select codigo from grupo where nome = 'Básico')),
	('ABC4567',2018,'2019-06-08',null,			53025,'3459847325ASD328',(select codigo from modelo where nome = 'Polo'), (select codigo from grupo where nome = 'Básico')),
	('CDE7654',2018,'2018-11-02','2020-01-07',	45800,'2344JH122H33GSS2',(select codigo from modelo where nome = 'Virtus'), (select codigo from grupo where nome = 'Básico')),
	('HGT4322',2020,'2020-04-22',null,			12345,'656456JH2H2G4333',(select codigo from modelo where nome = 'Celta'), (select codigo from grupo where nome = 'Básico')),
	('BVT2233',2018,'2019-02-13',null,			65832,'17233665KJKJ7567',(select codigo from modelo where nome = 'Cruze'), (select codigo from grupo where nome = 'Luxo')),
	('YTD2211',2017,'2017-09-29','2018-10-12',	95850,'2H43J234HG4J4J44',(select codigo from modelo where nome = 'Polo'), (select codigo from grupo where nome = 'Luxo')),
    ('AST5544',2017,'2017-10-19',null		,	98015,'5J36634HG4J4J36',(select codigo from modelo where nome = 'Golf'), (select codigo from grupo where nome = 'Luxo')),
	('FFT5544',2018,'2018-05-12','2020-03-12',	45676,'2H43J234HG4J4J11',(select codigo from modelo where nome = 'Ka'), (select codigo from grupo where nome = 'Luxo')),
	('UYY5533',2017,'2017-12-20',null,			20015,'323J234HG4J4J441',(select codigo from modelo where nome = 'Golf'), (select codigo from grupo where nome = 'Luxo'))
	;

insert into acessorio (sigla, nome)
values 
	('ar','ar condicionado'),
	('dh','direção hidráulica'),
	('ve','vidro elétrico'),
	('te','trava elétrica'),
	('cb','computador de bordo');

insert into veiculo_acessorio (placa_veiculo, sigla_acessorio)
values 
	('CDE7654','ar'),
	('CDE7654','dh'),
	('BVT2233','ar'),
	('BVT2233','dh'),
	('BVT2233','ve'),
	('BVT2233','te'),
	('YTD2211','ar'),
	('YTD2211','dh'),
	('YTD2211','ve'),
	('YTD2211','te');

insert into tabela_preco (codigo_grupo, data_inicio, data_fim, valor_diaria)
values 
	((select codigo from grupo where nome = 'Básico'),'2017-02-20','2019-05-18',80.00),
	((select codigo from grupo where nome = 'Básico'),'2019-05-18',null,95.00),
	((select codigo from grupo where nome = 'Luxo'),'2017-02-20','2019-05-18',140.50),
	((select codigo from grupo where nome = 'Luxo'),'2019-05-18','2020-03-02',170.00),
	((select codigo from grupo where nome = 'Luxo'),'20202-03-02',null,155.50);


