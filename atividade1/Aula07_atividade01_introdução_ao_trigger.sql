-- Trigger

CREATE OR REPLACE FUNCTION f_trg_upd_fabricante()
RETURNS trigger 
AS $$
BEGIN
	if TG_OP IN ('DELETE','UPDATE')  then
		raise notice 'estado anterior: %', OLD;
	end if;
	if TG_OP IN ('INSERT','UPDATE')  then
		raise notice 'novo estado: %', NEW;
	end if;
	
	if TG_OP IN ('INSERT','UPDATE') then
		return new;
	else
		return old;
	end if;
END;
$$ LANGUAGE plpgsql;

-- O controle de tamanho mínimo para o campo telefone é melhor implementado utilizando a constraint de check
-- alter table fabricante
-- 	add constraint ch_fabricante_telefone
-- 		check (length(telefone)>=8);

drop TRIGGER trg_upd_fabricante on fabricante;

CREATE TRIGGER trg_upd_fabricante
BEFORE UPDATE or INSERT or DELETE
ON fabricante
FOR EACH ROW
EXECUTE PROCEDURE f_trg_upd_fabricante();

select *
from fabricante;

-- OLD
-- "5BCE57920001A0"	"Veículos do Brasil SA"	"43563465     "	"Av. Feira"	"2769"	"Edf. Qualque Coisa"	"Liberdade"	"FDS"
-- "74779200015077"	"Tesla do Brasil SA"	"946756756    "	"Av. Bahia"	"5656"	"Edf. Uma Coisa"	"Moca"	"FDS"
update fabricante
set telefone = '71' || telefone
where cnpj in ('74779200015077','5BCE57920001A0');
-- NEW
-- "5BCE57920001A0"	"Veículos do Brasil SA"	"7143563465   "	"Av. Feira"	"2769"	"Edf. Qualque Coisa"	"Liberdade"	"FDS"
-- "74779200015077"	"Tesla do Brasil SA"	"71946756756  "	"Av. Bahia"	"5656"	"Edf. Uma Coisa"	"Moca"	"FDS"

-- OLD
-- "5BCE57920001A0"	"Veículos do Brasil SA"	"7143563465   "	"Av. Feira"	"2769"	"Edf. Qualque Coisa"	"Liberdade"	"FDS"
-- "74779200015077"	"Tesla do Brasil SA"	"71946756756  "	"Av. Bahia"	"5656"	"Edf. Uma Coisa"	"Moca"	"FDS"
update fabricante
set telefone = '55' || telefone
where cnpj in ('74779200015077','5BCE57920001A0');
-- NEW
-- "5BCE57920001A0"	"Veículos do Brasil SA"	"557143563465   "	"Av. Feira"	"2769"	"Edf. Qualque Coisa"	"Liberdade"	"FDS"
-- "74779200015077"	"Tesla do Brasil SA"	"5571946756756  "	"Av. Bahia"	"5656"	"Edf. Uma Coisa"	"Moca"	"FDS"

select CURRENT_USER, CURRENT_TIMESTAMP, CURRENT_TIME, CURRENT_DATE;

update fabricante
set telefone = '55123'
where cnpj = '5BCE57920001A0';

delete 
from fabricante
where cnpj = '5BCE57920001A0';

select *
from fabricante;

-- cod_item, qtd, valor_untario, valor_total
-- movimento (data, operacao[D|C], valor)
-- saldo atual = saldo inicial + acumulado de todas as operações (movimentos)