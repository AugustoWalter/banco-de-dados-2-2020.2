-- Listar a quantidade de veículos por ano de fabricação, mas apenas para os anos de fabricação com 3 ou mais veículos fabricados
-- e para veículos com menos de 98000 km rodados;

select ano_fabricacao, 
      count(*),
	  max(km_atual) --dessa forma eu descubro se o where está funcionando
from  veiculo
where km_atual < 98000  -- Nessa consulta é o primeiro a ser executado, avaliando cada tupla. O where se preocupa apenas em cada registro(tupla) por vez, não consegue lidar com grupos.
group by ano_fabricacao
having count(*) >= 3;  -- filtro sobre um resultado de uma função de agregação. É um 'where' especializado em resultado de função de agregação. 
                      
-- Listar os veículo.
-- Saída: placa, ano_fabricacao e o nome do modelo
-- Filtro: veículos com menos de 90000 km rodados.  

select veiculo.placa,
       veiculo.ano_fabricacao,
	   modelo.nome
from   veiculo,  -- produto cartesiano
       modelo
where km_atual < 90000
and   modelo.codigo = veiculo.codigo_modelo; -- critério de junção dos registros do produto cartesiano

select v.placa,
       v.ano_fabricacao,
	   m.nome
from veiculo v
     inner join modelo m on (m.codigo = v.codigo_modelo) -- como aqui os campos do criterio de junção são 'not null' o inner join é suficiente.	
where v.km_atual < 90000;	 
	 
-- Listar veículos e acessórios
-- Saída: placa, ano de fabricação, nome do modelo e o nome do acessório
-- Filtro: ano de fabricação maior ou igual que 2018;
-- Obs: Os veículos sem acessório devem ser retornados, o left aceita nularidade.
-- left, o que eu tenho na query da esquerda continua valendo e é preservado,
-- mesmo se os registros não atenderem a condição do left join.
-- Os registros que atenderem a condição é acrescentado à direita dos registros preservados.

select v.placa,
       v.ano_fabricacao,
	   m.nome,
     coalesce(a.nome, '(sem acessório)') as nome_acessorio
from veiculo v
     inner join modelo m on (m.codigo = v.codigo_modelo)
	 left join veiculo_acessorio va on (va.placa_veiculo = v.placa)
	 left join acessorio a on (a.sigla = va.sigla_acessorio)
where v.ano_fabricacao >= 2018;


select v.placa,
       v.ano_fabricacao,
	   m.nome,
     coalesce(a.nome, '(sem acessório)') as nome_acessorio
from veiculo_acessorio va
     inner join acessorio a on (a.sigla = va.sigla_acessorio)
	 right join veiculo v on (v.placa = va.placa_veiculo)
	 right join modelo m on (m.codigo = v.codigo_modelo)	
where v.ano_fabricacao >= 2018;


--  Listar Veículos e acessórios
-- Saída: placa ('(não instalado em nenhum veículo)') para acessórios que não estão instalados em nenhum veículo e
-- o nome do acessório ('(sem acessório)' para veículos que não possuem nenhum acessório);
-- full join cobre as especialidades do left e right, juntas. Usado quando precisa preservar os dois lados.

select coalesce(v.placa, '(Não instalado em nemhum veículo)') as placa,
       coalesce(a.nome, '(Sem acessório)') as nome_acessorio
from   veiculo v
       full join veiculo_acessorio va on (va.placa_veiculo = v.placa)
	   full join acessorio a on (a.sigla = va.sigla_acessorio);


-- Listar todos os modelos e todos os acessórios, combinando todas as possiblidades
-- é um plano cartesiano

select m.nome,
       a.sigla
from modelo m,
    acessorio a;

select m.nome,
       a.sigla
from   modelo m
       cross join acessorio a; -- geração de dado aleatorio, plano cartesiano.


-- USANDO NATURAL JOIN
-- Listar os veículo.
-- Saída: placa, ano_fabricacao e o nome do modelo
-- Filtro: veículos com menos de 90000 km rodados.

 alter table modelo
         rename column codigo to codigo_modelo;
		 -- ele verifica uma coluna que tenha o mesmo identificador em duas tabelas, e usa isso como criterio de junção.
		 -- não é muito recomendado usar.

select v.placa,
       v.ano_fabricacao,
	   m.nome
from veiculo v
     natural inner join modelo m 
where v.km_atual < 90000;


-- Listar os nomes e telefones, de funcionários e dependentes, construindo uma lista única, ordenado pela primeria coluna (nome) asc.

select nome,
       telefone
from   funcionario
union -- union all aceita registros duplicados, uma alternativa necessaria em algumas regras de negocio, como por exemplo: lista de chamada.
select nome,
       telefone
from   dependente
order by nome asc;
	   

-- Todos os acessórios que não estão instalados em nenhum veículo
-- Saída: sigla e o nome do acessório.

select a.sigla,
       a.nome
from acessorio a
where a.sigla not in (select va.sigla_acessorio    -- consulta aninhada
					 from veiculo_acessorio va);



-- Todos os acessórios que foram instalados em veículo
-- Saída: sigla e o nome do acessório.

select a.sigla,
       a.nome
from acessorio a
where a.sigla in (select va.sigla_acessorio    -- consulta aninhada
					 from veiculo_acessorio va);


	  	   
-- Solução que não comunica bem o objetivo da query:	   
select distinct a.sigla,
       a.nome
from acessorio a
inner join veiculo_acessorio va on (va.sigla_acessorio = a.sigla)
    
-- Solução que provoca uma queda de perfomance

select a.sigla,
       a.nome
from acessorio a
where exists(select *    -- consulta aninhada
			 from   veiculo_acessorio va
			 where  va.sigla_acessorio = a.sigla);
