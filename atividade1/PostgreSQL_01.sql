--Listar veículos:
--Colunas: placa, quilometragem atual e data de aquisição
--Filtro: adquiridos antes de 2015 e com quilometragem maior que 40000
--Ordenação: quilometragem atual (descendente) e placa (ascendente);


select placa, km_atual, data_aquisicao 
from veiculo
where data_aquisicao < '2019-01-01' 
and km_atual > 40000
order by km_atual desc, -- em caso de empate
         placa asc;



