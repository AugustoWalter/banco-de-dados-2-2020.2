--1. Listar veículos
--   Colunas: placa, quilometragem atual e data de aquisição
--   Filtro: adquiridos antes de 2019 com quilometragem maior que 40000
--   Ordenação: quilometragem atual (descendente) e placa (ascendente);
select	placa, km_atual, data_aquisicao
from	veiculo
where	data_aquisicao < '2019-01-01'
and		km_atual > 40000
order by km_atual desc,
		 placa asc;

--2. Listar modelos: 
--   Colunas: todas
--   Filtro: nome contém a palavra "gol" em qualquer local;
select	*
from	modelo
where	nome like '%gol%'; -- Não retorna nenhum registro, pois "gol" tem grafia como inicial maiúscula ("Gol","Golf").

select	*
from	modelo
where	nome ilike '%gol%'; -- ilike não considera diferenças de case (maiúsculas/minúsculas)

select	*
from	modelo
where	upper(nome) like '%GOL%'; -- se não tivermos o ilike no nosso SGBD.

--3. Listar fabricante:
--   Colunas: endereço (logradouro todo em minúsculo, primeiros 10 caracteres do complemento); nome do fabricante todo em maiúsculo;
--   Filtro: cnpj com menos de 14 caracteres (lembre-se de remover os caracteres em branco do início e fim do campo durante a consulta) 
--   ou que possuam caracteres não numéricos;
select 	lower(logradouro) || ' - ' || substr(complemento, 1, 10) as endereco,
		upper(nome)
from	fabricante
where	length(trim(cnpj)) < 14
or		cnpj ~ '[^\d]';

select 	concat(lower(logradouro), ' - ', substr(complemento, 1, 10)) as endereco,
		upper(nome)
from	fabricante
where	length(trim(cnpj)) < 14
or		cnpj ~ '[^\d]';

--4. Listar veículos:
--   Colunas: quantidade, quilometragem do mais rodado; quilometragem do menos rodado; quilometragem média; 
--   total de quilômetros rodados por todos os veículos
--   Filtro: nenhum;
select	count(*)		as qtd_veiculos,
		max(km_atual)	as qtd_km_veiculo_mais_rodado,
		min(km_atual)	as qtd_km_veiculo_menos_rodado,
		avg(km_atual)	as qtd_km_medio,
		sum(km_atual)	as qtd_km_total
from	veiculo;

--5. Listar veículos:
--   Colunas: ano de fabricação, quantidade; quilometragem do mais rodado; quilometragem do menos rodado; 
--   quilometragem média; total de quilômetros rodados por todos os veículos
--   Filtro: nenhum.
select	ano_fabricacao,
		count(*)			as qtd_veiculos1,
		count(placa)		as qtd_veiculos2,
		count(data_venda)	as qtd_veiculos3,
		max(km_atual)	as qtd_km_veiculo_mais_rodado,
		min(km_atual)	as qtd_km_veiculo_menos_rodado,
		avg(km_atual)	as qtd_km_medio,
		sum(km_atual)	as qtd_km_total
from	veiculo
group by ano_fabricacao;

-- Para lembrar a diferença entre count(*) e count(atributo), este último não conta valores nulos e o primeiro conta a quantidade de tuplas.
select	ano_fabricacao,
		count(*)			as qtd_veiculos1,
		count(placa)		as qtd_veiculos2,
		count(data_venda)	as qtd_veiculos_vendidos,
		max(km_atual)	as qtd_km_veiculo_mais_rodado,
		min(km_atual)	as qtd_km_veiculo_menos_rodado,
		avg(km_atual)	as qtd_km_medio,
		sum(km_atual)	as qtd_km_total
from	veiculo
group by ano_fabricacao;

-- Operadores aritméticos: + - / *
-- Operadores relacionais: <  <= > >= <>
-- Operadores lógicos: and or not

--6. Listar as distintas siglas de acessórios que estão instalados em algum veículo:
--   Colunas: sigla_acessorio
--   Filtro: nenhum;
select distinct sigla_acessorio
from	veiculo_acessorio;

--7. Veículos e suas respectivas datas de venda. Para os veículos não vendidos, exibir a mensagem '(não vendido)':
-- 	Colunas: placa, data_venda (mensagem '(não vendido)' para as data_venda nulas)
--	Filtro: nenhum;
select	placa,
		coalesce(cast(data_venda as varchar),'(não vendido)') as data_venda
from 	veiculo;

select	placa,
		coalesce (data_venda, '2008-01-0)') as data_venda
from 	veiculo;


--coalesce(atributo, valor-default): valor-atributo == null ? valor-default : valor-atributo
--nvl(atributo, valor-default)
--isnull(atributo, valor-default)



