-- Listar veículos:
-- Colunas:  ano de fabricação, quantidade;
-- quilometragem do mais rodado;
-- quilometragem do menos rodado;
--  quilometragem média;
-- total de quilômetros rodados por todos os veículos.


select ano_fabricacao, count(*) as qtd_veiculos,
max(km_atual) as km_max,	
min(km_atual) as km_min,
avg(km_atual) as km_media,
sum(km_atual) as km_total
from veiculo
group by ano_fabricacao;
