-- Atividade01: Lucas Augusto; Walter Frota.


CREATE OR REPLACE FUNCTION f_trg_modelo_alteracoes() 
RETURNS TRIGGER
AS $$
BEGIN
  IF (TG_OP = 'DELETE') THEN
    INSERT INTO auditoria_delet_modelo values (default, 'D', user, now(), OLD.nome);
    RETURN OLD;
    ELSIF (TG_OP = 'UPDATE') THEN
    INSERT INTO auditoria_update_modelo  values (default , 'U', user, now(), NEW.nome, OLD.nome);
    RETURN NEW;
    ELSIF (TG_OP = 'INSERT') THEN
    INSERT INTO auditoria_insert_modelo values (default, 'I', user, now(), NEW.nome);
    RETURN NEW;
  END IF;
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;


CREATE TRIGGER trg_modelo_alteracoes
BEFORE UPDATE or INSERT or DELETE
ON modelo
FOR EACH ROW
EXECUTE PROCEDURE f_trg_modelo_alteracoes();



update modelo
set nome = 'S10'
where codigo_modelo = 9;

update modelo
set nome = 'Prisma'
where codigo_modelo = 10;

delete
from modelo
where codigo_modelo = 11;

insert into modelo (nome, cnpj_fabricante)
values 
	('Tracker','59275792000150');

create table auditoria_update_modelo (
	codigo_update       serial          not null,
	operacao			char(1)			not null,
	usuario             text            not null,
	dataHora			timestamp		not null,
	modelo_new          varchar(40)			    ,
	modelo_old          varchar(40)             ,
	constraint pk_auditoria_update_modelo
		primary key (codigo_update));
		
	
	
	create table auditoria_insert_modelo (
	codigo_insert       serial          not null, 
	operacao			char(1)			not null,
	usuario             text            not null,
	dataHora			timestamp		not null,	
	modelo_new          varchar(40)			    ,
	constraint pk_auditoria_insert_model
		primary key (codigo_insert));
		
	create table auditoria_delet_modelo (
	codigo_delete       serial          not null,
	operacao			char(1)			not null,
	usuario             text            not null,
	dataHora			timestamp		not null,	
	modelo_old          varchar(40)			    ,
	constraint pk_auditoria_delet_modelo
		primary key (codigo_delete));



create or replace view v_audit_modelo_delet
as
select 
       codigo_delete,
       operacao,
       usuario,
	   datahora,
	   modelo_old
from auditoria_delet_modelo;

select *
from v_audit_modelo_delet;



create or replace view v_audit_modelo_insert
as
select 
       codigo_insert,
       operacao,
       usuario,
	   datahora,
	   modelo_new
from auditoria_insert_modelo

select *
from v_audit_modelo_insert;



create or replace view v_audit_modelo_update
as
select 
       codigo_update,
       operacao,
       usuario,
	   datahora,
	   modelo_new,
	   modelo_old
from auditoria_update_modelo

select *
from v_audit_modelo_update;




